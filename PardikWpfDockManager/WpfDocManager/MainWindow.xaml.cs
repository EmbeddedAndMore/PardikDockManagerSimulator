﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;

namespace WpfDocManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        System.Timers.Timer timer;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            timer = new System.Timers.Timer(250);
            timer.Elapsed += timer_Elapsed;
            timer.Start();
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                try
                {
                    
                    dockManager1.SaveLayoutToXml(@"C:\Users\mohamad\Desktop\Pardik\MyLayout.xml");
                    Dimen d = new Dimen() { Width = (int)this.Width, Height = (int)this.Height};
                    XmlSerializer serializer = new XmlSerializer(typeof(Dimen));
                    using(XmlWriter writer = XmlWriter.Create(@"C:\Users\mohamad\Desktop\Pardik\Dimen.xml"))
                    {
                        serializer.Serialize(writer, d);
                    }
                }
                catch (Exception)
                {               }
            });
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            timer.Stop();
        }
    }
}
