﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DockPanels
{
    class MyPanelModel
    {
        public System.Windows.Forms.Panel panel { get; set; }
        public bool IsActive { get; set; }
    }
}
