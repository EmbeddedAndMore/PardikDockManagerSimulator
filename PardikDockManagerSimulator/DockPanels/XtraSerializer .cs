﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DockPanels
{
    [XmlRoot(ElementName = "XtraSerializer")]
    public class XtraSerializer
    {
        [XmlElement(ElementName = "property")]
        public List<Property> Property { get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }
        [XmlAttribute(AttributeName = "application")]
        public string Application { get; set; }
    }

    [XmlRoot(ElementName = "property")]
    public class Property
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "isnull")]
        public string Isnull { get; set; }
        [XmlElement(ElementName = "property")]
        public List<Property> Properties { get; set; }
        [XmlAttribute(AttributeName = "iskey")]
        public string Iskey { get; set; }
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlText]
        public string StringValue { get; set; }
    }
}
