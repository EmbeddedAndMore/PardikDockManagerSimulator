﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DockPanels
{
    public class PanelInfo
    {
        public string Name { get; set; }
        public string ParentName { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int NumOfChild { get; set; }
        public int LocX { get; set; }
        public int LocY { get; set; }
        public string Type { get; set; }
        public string Orientation { get; set; }
    }
}
