﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DockPanels
{
    class Orientation
    {
        public static string H { get { return "Horizontal"; } }
        public static string V { get { return "Vertical"; } }
    }
}
