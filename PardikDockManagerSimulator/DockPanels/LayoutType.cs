﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DockPanels
{
    class LayoutType
    {
        public static string Group { get { return "LayoutGroup";} }
        public static string Panel { get { return "LayoutPanel";} }
        public static string Float { get {return "FloatGroup";} }
    }
}
