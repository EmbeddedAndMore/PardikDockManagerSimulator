﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Drawing;

namespace DockPanels
{
    public class DockPanels : UserControl
    {
        public string XmlFilePath { get; set; }

        private Random rnd;
        private List<LayoutParameters> parsedLayouts;
        private List<PanelInfo> LatestPanelInfos;
        private bool ShouldUpdate;
        private int runCounter;
        public DockPanels()
        {
            this.SizeChanged += DockPanels_SizeChanged;

            rnd = new Random();
            parsedLayouts = new List<LayoutParameters>();
            LatestPanelInfos = new List<PanelInfo>();
            ShouldUpdate = false;
            runCounter = 0;
           
        }

        public void Start()
        {
            try
            {
                runCounter++;
                XmlSerializer serializer = new XmlSerializer(typeof(XtraSerializer));
                string XmlString = File.ReadAllText(XmlFilePath);
                TextReader reader = new StringReader(XmlString);
                XtraSerializer deserialized = (XtraSerializer)serializer.Deserialize(reader);

                foreach (Property prop1 in deserialized.Property)
                {
                    if (prop1.Name == "$DockLayoutManager")
                    {
                        foreach (Property prop2 in prop1.Properties)
                        {
                            if (prop2.Name == "Items")
                            {
                                ParseLayoutPanels(prop2.Properties);
                            }
                        }
                    }
                }
                ShouldUpdate = true;
            }
            catch (Exception ex)
            {

            }
        }

        private void ParseLayoutPanels(List<Property> items)
        {
            parsedLayouts.Clear();
            
            foreach (Property layout in items)
            {
                LayoutParameters param = new LayoutParameters();
                param.TypeName = layout.Properties.Where(x => x.Name == "TypeName").First().StringValue;
                LayoutPanelParameter lpParam = new LayoutPanelParameter();
                LayoutGroupParameter lgParam = new LayoutGroupParameter();
                FloatGroupParameter fgParam = new FloatGroupParameter();
                foreach (Property Parameter in layout.Properties)
                {
                    if (Parameter.Name == "TypeName")
                        continue;

                    if (Parameter.Name == "ParentName")
                        param.ParentName = Parameter.StringValue;
                    else if (Parameter.Name == "ParentCollectionName")
                        param.ParentCollectionName = Parameter.StringValue;
                    else if (Parameter.Name == "Name")
                        param.Name = Parameter.StringValue;
                    else if (Parameter.Name == "Caption")
                        param.Caption = Parameter.StringValue;
                    else if (Parameter.Name == "SerializableDockSituation")
                    {
                        param.SerializableDockSituation = Parameter.StringValue;
                        if (Parameter.Value == "1")
                        {
                            List<Property> parents = Parameter.Properties.First().Properties;
                            foreach (Property parent in parents)
                            {
                                if (parent.Name == "DockState")
                                    lpParam.DockState = parent.StringValue;
                                else if (parent.Name == "Index")
                                    lpParam.Index = parent.StringValue;
                                else if (parent.Name == "OwnerName")
                                    lpParam.OwnerName = parent.StringValue;
                            }
                        }
                    }


                    if (param.TypeName == LayoutType.Group)
                    {
                        if (Parameter.Name == "SerializableSelectedTabPageIndex")
                            lgParam.SerializableSelectedTabPageIndex = Parameter.StringValue;

                        else if (Parameter.Name == "Orientation")
                            lgParam.Orientation = Parameter.StringValue;
                        else if (Parameter.Name == "ItemHeight")
                            lgParam.ItemHeight = Parameter.StringValue;
                        else if (Parameter.Name == "ItemWidth")
                            lgParam.ItemWidth = Parameter.StringValue;
                    }
                    else if (param.TypeName == LayoutType.Panel)
                    {
                        if (Parameter.Name == "SerializableFloatingBounds")
                            lpParam.SerializableFloatingBounds = Parameter.StringValue;
                        else if (Parameter.Name == "SerializableFloatingOffset")
                            lpParam.SerializableFloatingOffset = Parameter.StringValue;
                        else if (Parameter.Name == "ItemWidth")
                            lpParam.ItemWidth = Parameter.StringValue;
                        else if (Parameter.Name == "FloatSize")
                            lpParam.FloatSize = Parameter.StringValue;
                        else if (Parameter.Name == "IsActive")
                            lpParam.IsActive = Parameter.StringValue;
                        else if (Parameter.Name == "ItemHeight")
                            lpParam.ItemHeight = Parameter.StringValue;
                    }
                    else if (param.TypeName == LayoutType.Float)
                    {
                        if (Parameter.Name == "FloatLocation")
                            fgParam.FloatLocation = Parameter.StringValue;
                        else if (Parameter.Name == "ShouldRestoreOnActivate")
                            fgParam.ShouldRestoreOnActivate = Parameter.StringValue;
                        else if (Parameter.Name == "SerializableIsMaximized")
                            fgParam.SerializableIsMaximized = Parameter.StringValue;
                        else if (Parameter.Name == "SerializableZOrder")
                            fgParam.SerializableZOrder = Parameter.StringValue;
                        else if (Parameter.Name == "FloatSize")
                            fgParam.FloatSize = Parameter.StringValue;
                    }
                }
                if (param.TypeName == LayoutType.Group)
                    param.LayoutGroup = lgParam;
                else if (param.TypeName == LayoutType.Panel)
                    param.LayoutPanel = lpParam;
                else if (param.TypeName == LayoutType.Float)
                    param.FloatPanel = fgParam;

                parsedLayouts.Add(param);
            }
            DrawPanels(this.Width, this.Height);
        }

        private void DrawPanels(int ParentWidth, int ParentHeight)
        {
            

            this.BackColor = Color.LightGray;
            List<MyPanelModel> StackPanels = new List<MyPanelModel>();
            LayoutParameters MainGroup = parsedLayouts[0];


            int TotalWidth = this.Width;
            int TotalHeight = this.Height;
            int WidthOffset = 0;
            int HeightOffset = 0;


            List<PanelInfo> PanelInfos = new List<PanelInfo>();
            PanelInfo firstInfo = new PanelInfo();
            firstInfo.Name = parsedLayouts[0].Name;
            firstInfo.NumOfChild = parsedLayouts.Where(x => x.ParentName == parsedLayouts[0].Name).Count();
            firstInfo.ParentName = null;
            firstInfo.Width = this.Width;
            firstInfo.Height = this.Height;
            firstInfo.LocX = 0;
            firstInfo.LocY = 0;
            firstInfo.Type = LayoutType.Group;
            firstInfo.Orientation = (string.IsNullOrEmpty(parsedLayouts[0].LayoutGroup.Orientation) ||
                                parsedLayouts[0].LayoutGroup.Orientation == Orientation.H) ? Orientation.H : Orientation.V;
            PanelInfos.Add(firstInfo);

            
            foreach (LayoutParameters layout in parsedLayouts)
            {
                if (layout.Name != MainGroup.Name)
                {
                    PanelInfo inf = new PanelInfo();
                    inf.Name = layout.Name;
                    if (layout.TypeName == LayoutType.Group)
                    {
                        int NumberOfChilds = parsedLayouts.Where(x => x.ParentName == layout.Name).Count();
                        PanelInfo parent = PanelInfos.Where(x => x.Name == layout.ParentName).First();
                        inf.ParentName = parent.Name;
                        inf.NumOfChild = NumberOfChilds;
                        inf.Type = LayoutType.Group;
                        inf.Orientation = (string.IsNullOrEmpty(layout.LayoutGroup.Orientation) ||
                                layout.LayoutGroup.Orientation == Orientation.H) ? Orientation.H : Orientation.V;


                        int height = 0, width = 0;
                        if (parent.Orientation == Orientation.H)
                        {
                            double ItemWidthCoef = 1;
                            if (!string.IsNullOrEmpty(layout.LayoutGroup.ItemWidth))
                            {
                                string ItemWidthCoef_S = layout.LayoutGroup.ItemWidth.Replace("*", "");
                                if (ItemWidthCoef_S != "")
                                    double.TryParse(ItemWidthCoef_S, out ItemWidthCoef);
                            }
                            width = (int)((parent.Width / parent.NumOfChild) * ItemWidthCoef);
                            height = parent.Height;

                        }

                        if (parent.Orientation == Orientation.V)
                        {
                            double ItemHeightCoef = 1;
                            if (!string.IsNullOrEmpty(layout.LayoutGroup.ItemHeight))
                            {
                                string ItemHeightCoef_S = layout.LayoutGroup.ItemHeight.Replace("*", "");
                                if (ItemHeightCoef_S != "")
                                    double.TryParse(ItemHeightCoef_S, out ItemHeightCoef);
                            }
                            width = parent.Width;
                            height = (int)((parent.Height / parent.NumOfChild) * ItemHeightCoef);
                        }
                        inf.Width = width;
                        inf.Height = height;



                        WidthOffset = parent.LocX;
                        HeightOffset = parent.LocY;
                        List<PanelInfo> SameLeveChiled = PanelInfos.Where(x => x.ParentName == parent.Name).ToList();
                        foreach (PanelInfo child in SameLeveChiled)
                        {
                            if (parent.Orientation == Orientation.H)
                                WidthOffset += child.Width;
                            if (parent.Orientation == Orientation.V)
                                HeightOffset += child.Height;
                        }
                        inf.LocX = WidthOffset;
                        inf.LocY = HeightOffset;


                        PanelInfos.Add(inf);
                    }
                    else if (layout.TypeName == LayoutType.Panel)
                    {
                        PanelInfo parent = PanelInfos.Where(x => x.Name == layout.ParentName).First();
                        inf.ParentName = parent.Name;
                        inf.NumOfChild = 0;
                        inf.Type = LayoutType.Panel;
                        inf.Orientation = null;

                        

                        int height = 0, width = 0;
                        if (parent.Orientation == Orientation.H)
                        {
                            double ItemWidthCoef = 1;
                            if (!string.IsNullOrEmpty(layout.LayoutPanel.ItemWidth))
                            {
                                string ItemWidthCoef_S = layout.LayoutPanel.ItemWidth.Replace("*", "");
                                if (ItemWidthCoef_S != "")
                                    double.TryParse(ItemWidthCoef_S, out ItemWidthCoef);
                            }
                            width = (int)((parent.Width / parent.NumOfChild) * ItemWidthCoef);
                            height = parent.Height;
                        }
                        if (parent.Orientation == Orientation.V)
                        {
                            double ItemHeightCoef = 1;
                            if (!string.IsNullOrEmpty(layout.LayoutPanel.ItemHeight))
                            {
                                string ItemHeightCoef_S = layout.LayoutPanel.ItemHeight.Replace("*", "");
                                if (ItemHeightCoef_S != "")
                                    double.TryParse(ItemHeightCoef_S, out ItemHeightCoef);
                            }
                            width = parent.Width;
                            height = (int)((parent.Height / parent.NumOfChild) * ItemHeightCoef);
                        }
                        inf.Width = width;
                        inf.Height = height;



                        WidthOffset = parent.LocX;
                        HeightOffset = parent.LocY;
                        List<PanelInfo> SameLeveChiled = PanelInfos.Where(x => x.ParentName == parent.Name).ToList();
                        foreach (PanelInfo child in SameLeveChiled)
                        {
                            if (parent.Orientation == Orientation.H)
                                WidthOffset += child.Width;
                            if (parent.Orientation == Orientation.V)
                                HeightOffset += child.Height;
                        }
                        inf.LocX = WidthOffset;
                        inf.LocY = HeightOffset;


                        PanelInfos.Add(inf);
                    }
                }
            }
            int count = this.Controls.Count;
            List<Control> ExcessiveContorls = new List<Control>();
            if(runCounter >1)
            {
                foreach (Control c in this.Controls)
                {
                    PanelInfo existingPanel = PanelInfos.Where(x => x.Name == c.Name && x.Type == LayoutType.Panel).FirstOrDefault();
                    if (existingPanel == null)
                        ExcessiveContorls.Add(c);
                }
                foreach (Control c in ExcessiveContorls)
                {
                    this.Controls.Remove(c);
                }
            }
            foreach (PanelInfo info in PanelInfos)
            {
                if (info.Type == LayoutType.Panel)
                {
                    PanelInfo LastOne = LatestPanelInfos.Where(x => x.Name == info.Name).FirstOrDefault();
                    if (LastOne != null)
                    {
                        int ControlCount = this.Controls.Count;
                        Panel existingPanel = (Panel)this.Controls.Find(LastOne.Name, false)[0];
                        if (this.RightToLeft == RightToLeft.No)
                        {
                            existingPanel.Location = new Point(info.LocX, info.LocY);
                        }
                        else if (this.RightToLeft == RightToLeft.Yes)
                        {
                            existingPanel.Location = new Point(this.Width - info.Width - info.LocX, info.LocY);
                        }
                        existingPanel.Size = new Size(info.Width, info.Height);
                    }
                    else
                    {

                        Panel pnl = new Panel();
                        pnl.BackColor = GenRandColor();
                        if (this.RightToLeft == RightToLeft.No)
                            pnl.Location = new Point(info.LocX, info.LocY);
                        else if (this.RightToLeft == RightToLeft.Yes)
                            pnl.Location = new Point(this.Width - info.Width - info.LocX, info.LocY);
                        pnl.Width = info.Width;
                        pnl.Height = info.Height;
                        pnl.Name = info.Name;
                        pnl.Click += pnl_Click;
                        this.Controls.Add(pnl);
                    }
                }
            }
            LatestPanelInfos.Clear();
            LatestPanelInfos.AddRange(PanelInfos.Where(x=>x.Type == LayoutType.Panel).ToList());

        }
        void pnl_Click(object sender, EventArgs e)
        {
            Panel selected = (Panel)sender;
            foreach (Control panel in this.Controls)
                ((Panel)panel).BorderStyle = BorderStyle.None;
            selected.BorderStyle = BorderStyle.Fixed3D;
        }

        private Color GenRandColor()
        {
            Color randomColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
            return randomColor;
        }
        void DockPanels_SizeChanged(object sender, EventArgs e)
        {
            if (ShouldUpdate)
            {
                DrawPanels(this.Width, this.Height);
            }
        }

    }



}
