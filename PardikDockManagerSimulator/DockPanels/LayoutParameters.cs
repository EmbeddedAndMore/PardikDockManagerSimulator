﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DockPanels
{
    public class LayoutParameters
    {
        public string Name { get; set; }
        public string TypeName { get; set; }
        public string ParentName { get; set; }
        public string ParentCollectionName { get; set; }
        public string Caption { get; set; }
        public string SerializableDockSituation { get; set; }
        public LayoutGroupParameter LayoutGroup { get; set; }
        public LayoutPanelParameter LayoutPanel { get; set; }
        public FloatGroupParameter FloatPanel { get; set; }
        
    }

    public class LayoutGroupParameter
    {
        public string SerializableSelectedTabPageIndex { get; set; }
        public string Orientation { get; set; }
        public string ItemWidth { get; set; }
        public string ItemHeight { get; set; }
    }
    public class LayoutPanelParameter
    {
        public string SerializableFloatingBounds { get; set; }
        public string SerializableFloatingOffset { get; set; }
        public string ItemWidth { get; set; }
        public string ItemHeight { get; set; }
        public string FloatSize { get; set; }
        public string IsActive { get; set; }
        public string DockState { get; set; }
        public string OwnerName { get; set; }
        public string Index { get; set; }
        public int DrawOrder { get; set; }
    }

    public class FloatGroupParameter
    {
        public string FloatLocation { get; set; }
        public string ShouldRestoreOnActivate { get; set; }
        public string SerializableIsMaximized { get; set; }
        public string SerializableZOrder { get; set; }
        public string FloatSize { get; set; }
    }
}
